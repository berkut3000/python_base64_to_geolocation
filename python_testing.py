import base64
import sys
if sys.version_info[0] > 2:
    import codecs
# from struct import pack as spack
# a1=spack("LLLL", 45129401,92367215,681285731,1710201)
# print(type(a1))
# print(codecs.encode(a1, 'hex_codec'))
coded_string = '''gBRZmmxkO49C'''
a = base64.b64decode(coded_string)

if sys.version_info[0] > 2:
    b = codecs.encode(a, 'hex_codec')
    var_orientacion_ns = (a[0] & 0x80)>>7
    var_orientacion_oe = (a[0] & 0x40)>>6
    num_latitud = str(a[1])
    dec_latitud = str((a[2]<<16) + (a[3]<<8) + a[4])
    num_longitud = str(a[5])
    dec_longitud = str((a[6]<<16) + (a[7]<<8) + a[8])

else:
    b = a.encode("hex")
    var_orientacion_ns = (ord(a[0]) & 0x80)>>7
    var_orientacion_oe = (ord(a[0]) & 0x40)>>6
    num_latitud = str(ord(a[1]))
    dec_latitud = str(int("0x" + a[2:5].encode("hex"),0))
    num_longitud = str(ord(a[5]))
    dec_longitud = str(int("0x" + a[6:9].encode("hex"),0))

# print(ord(a[8]))

if var_orientacion_ns == 1:
    norte = " "
else:
    norte = "-"

if var_orientacion_oe == 1:
    oeste = " "
else:
    oeste = "-"

cadena_resultante = norte + num_latitud + "." + dec_latitud + "," + oeste + num_longitud + "." + dec_longitud
# print(var_orientacion_ns)
# print(var_orientacion_oe)
# print(num_latitud)
# print(dec_latitud)
#
#
# print(num_longitud)
# print(dec_longitud)
#
# print(ord(a[0]))
#
#
print(b)
print(cadena_resultante)
